-module(box).
-export([start/1]).
-export([loop/1]).

start(State) ->
  spawn(?MODULE, loop, [State]).

loop(State) ->
  receive
    {set, NewVal} ->
      NewState = NewVal;
    {Pid, get} ->
      Pid ! {value, State},
      NewState = State
  end,
  loop(NewState).

