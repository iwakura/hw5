-module(newaddress).
-export([newaddress/3]).
-include("salesdata.hrl").
-import(salesdata, [store/2, group/2]).

-spec newaddress(salesdata:salesdata(), string(), string()) -> salesdata:salesdata().

newaddress(SD, New, Old) when is_record(SD, store), Old == SD#store.address ->
  SD#store{address=New};

newaddress(SD, _New, _Old) when is_record(SD, store) ->
  SD;

newaddress(SD, New, Old) when is_record(SD, group) ->
  SD#group{members=[newaddress(Member, New, Old) || Member <- SD#group.members]}.

