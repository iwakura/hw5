-module(eventdetector).
-export([start/2, loop/2]).

start(InitialState, TransitionFun) ->
  spawn(?MODULE, loop, [InitialState, TransitionFun]).

loop(InitialState, TransitionFun) ->
  loop(InitialState, TransitionFun, []).

loop(State, TransitionFun, Observers) ->
  receive
    {Pid, add_me} ->
      Pid ! {added},
      loop(State, TransitionFun, [Pid | Observers]);
    {Pid, add_yourself_to, EDPid} ->
      EDPid ! {self(), add_me},
      receive
        {added} -> Pid ! {added}
      end,
      loop(State, TransitionFun, Observers);
    {Pid, state_value} ->
      Pid ! {value_is, State},
      loop(State, TransitionFun, Observers);
    Signal ->
      NewState =
      case TransitionFun(State, Signal) of
        {NS, none} ->
          NS;
        {NS, Event} ->
          notify_observers(Observers, Event),
          NS
      end,
      loop(NewState, TransitionFun, Observers)
  end.

notify_observers(Observers, Event) ->
  [Observer ! Event || Observer <- Observers].

