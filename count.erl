-module(count).
-export([count/2]).

-spec count (char(), string()) -> integer().
count(What, S) ->
  count_char(What, S, 0).

count_char(_What, [], Count) ->
  Count;
count_char(What, [What|Rest], Count) ->
  count_char(What, Rest, Count + 1);
count_char(What, [_|Rest], Count) ->
  count_char(What, Rest, Count).

