-module(future).
-export([makeFuture/2,futureValue/1]).
-export([exec/2]).
-export_type([future/1]).
-type future(T) :: {future, pid(), T}.

makeFuture(P, ErrValue) ->
  Pid = spawn(?MODULE, exec, [P, ErrValue]),
  {future, Pid, ErrValue}.

futureValue({future, Pid, _ErrValue}) ->
  Pid ! {result, self()},
  receive
    {result, Res} ->
      Res
  end.

exec(Fun, ErrValue) ->
  Res =
  try Fun()
  catch
    _:_ -> ErrValue
  end,
  receive
    {result, Pid} ->
      Pid ! {result, Res}
  end.
