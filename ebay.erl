-module(ebay).
-export([start/1]).
-export([auction/1]).

start(Item) ->
  io:format("Starting auction for ~p~n", [Item]),
  spawn(?MODULE, auction, [Item]).

auction(Item) ->
  loop(Item, active, []).

loop(Item, Status, Bids) ->
  receive
    {Pid, finish} ->
      NewStatus = finished,
      NewBids = Bids,
      Pid ! auction_closed,
      notify_bidders(Bids);
    {Pid, bid, Amt, Info} ->
      case Status of
        finished ->
          Pid ! auction_is_over,
          NewBids = Bids;
        _ ->
          NewBids = [{Pid, Amt, Info} | Bids]
      end,
      NewStatus = Status;
    {Pid, who_won} ->
      case Status of
        finished ->
          case winner(Bids) of
            {_Pid, _Amt, Info} ->
              Pid ! Info;
            _ ->
              Pid ! no_one
          end;
        _ ->
          ok
      end,
      NewStatus = Status,
      NewBids = Bids
  end,
  loop(Item, NewStatus, NewBids).

notify_bidders([]) ->
  ok;
notify_bidders(Bids) ->
  Winner = winner(Bids),
  [notify_bidder(Bid, Winner) || Bid <- Bids].

notify_bidder({Pid, Amt, _Info}, {Pid, Amt, _Info}) ->
  Pid ! {you_won, Amt};
notify_bidder({Pid, _Amt, _Info}, _Winner) ->
  Pid ! sorry.


winner([]) ->
  nil;
winner(Bids) ->
  MaxBid = lists:max([A || {_P, A, _I} <- Bids]),
  lists:keyfind(MaxBid, 2, lists:reverse(Bids)).

